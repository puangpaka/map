from django.contrib import admin
from django.urls import path
from appmapmaker import views,pushdata

urlpatterns = [
    path('',views.index),
    path('home/',views.home),
    path('admin/', admin.site.urls),
    #path('pushdata/',pushdata.readm),
]
