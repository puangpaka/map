from django.db import models

class Data_School(models.Model):
    Id = models.CharField(max_length=100,primary_key=True)
    Name = models.CharField(max_length=100,default=' ')
    Address = models.CharField(max_length=100,default=' ')
    Province = models.CharField(max_length=100,default=' ')
    Status = models.CharField(max_length=100,default=' ')
    Register_date = models.CharField(max_length=100,default=' ')
    Award_date = models.CharField(max_length=100,default=' ')
    Category = models.CharField(max_length=100,default=' ')

    Latitude = models.FloatField()
    Longitude = models.FloatField()

    Picture_1 = models.CharField(max_length=100,default=' ')
    Picture_2 = models.CharField(max_length=100,default=' ')
    Picture_3 = models.CharField(max_length=100,default=' ')

    def __str__(self):
      return f'{self.Id} {self.Name} {self.Status} '
    
    def get_name(self):
      return f'{self.Name}'