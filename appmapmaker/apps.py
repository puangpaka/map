from django.apps import AppConfig


class AppmapmakerConfig(AppConfig):
    name = 'appmapmaker'
