from django.shortcuts import render
from .models import Data_School
import json

# Create your views here.
def index(req):
    return render(req,'index.html')


def home(req):
    users = Data_School.objects.all().values('Name', 'Latitude','Longitude','Status','Category')  # or simply .values() to get all fields
    print(users)
    return render(req,'home.html',{'x':users})